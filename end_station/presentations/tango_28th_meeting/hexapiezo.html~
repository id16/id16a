<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta name="viewport" content="width=1024, user-scalable=no">

  <title>ID16 Piezo Hexapod Presentation</title>

  <!-- Required stylesheet -->
  <link rel="stylesheet" media="screen" href="deck.js/core/deck.core.css">

  <!-- Extension CSS files go here. Remove or add as needed. -->
  <link rel="stylesheet" media="screen" href="deck.js/extensions/goto/deck.goto.css">
  <link rel="stylesheet" media="screen" href="deck.js/extensions/menu/deck.menu.css">
  <link rel="stylesheet" media="screen" href="deck.js/extensions/navigation/deck.navigation.css">
  <link rel="stylesheet" media="screen" href="deck.js/extensions/status/deck.status.css">
  <link rel="stylesheet" media="screen" href="deck.js/extensions/scale/deck.scale.css">

  <!-- Style theme. More available in /themes/style/ or create your own. -->
  <link rel="stylesheet" media="screen" href="deck.js/themes/style/esrf.css">

  <!-- Transition theme. More available in /themes/transition/ or create your own. -->
  <link rel="stylesheet" media="screen" href="deck.js/themes/transition/horizontal-slide.css">

  <!-- Basic black and white print styles -->
  <link rel="stylesheet" media="print" href="deck.js/core/print.css">

  <!-- Required Modernizr file -->
  <script src="deck.js/modernizr.custom.js"></script>


</head>
<body>
  <div class="deck-container">






    <!-- Begin slides. Just make elements with a class of slide. -->

    <section class="slide" id="title">
      <h1>ID16 PiezoHexapod</h1>
        <h3> Cyril Guilloud</h3>
        <h3> Manuel Perez</h3>
        <h3> Jens Meyer</h3>
        <h4>BLISS - ESRF</h4>
        <h5>28th Tango meeting - ESRF - Grenoble</h5>
    </section>







    <section class="slide" id="design">
      <h2>Imaging Sample Stage</h2>

    <h3> ID16 nano-imaging Beamline</h3>
<ul>
  <li class="slide" >
    Tomography  <img src="tomo-synoptic.png" alt="tomo">
  </li>
  <li class="slide" >
    Mapping  <img src="mapping-synoptic.png" alt="tomo">
  </li>
</ul>

</section>









    <section class="slide" id="hardware">
      <h2>Many devices to interface</h2>
<ul>
  <li class="slide" id="flexdc">
    <strong>Flexdc</strong> nanomotion piezo rotation for tomography
  </li>
  <li class="slide" id="pie517">
    <strong>PI E517</strong> 2x3 axis piezo controller for hexapod
  </li>
  <li class="slide" id="adc">
    <strong>Adlink PCI ADC card</strong> for capacitive captors reading
  </li>
  <li class="slide" id="piezo">
    <strong>Piezomotor PMD206 </strong> for alignment
  </li>
</ul>

<h2 class="slide" >Feedback loop to control all this stuff</h2>
<ul>
  <li class="slide" id="">
    <strong>Calculs</strong> 
      <ul>
        <li class="slide" id=""> Legs to cartesian coodinates (matrix calculation) </li>
        <li class="slide" id=""> capacitive captors to cart. coord. </li>
        <li class="slide" id=""> Correction of cart. position relative to setpoint </li>
        <li class="slide" id=""> Correction to legs lengths </li>
      </ul>
  </li>
  <li class="slide" id="pie517">
    <strong>Constraints </strong> 30 Hz
  </li>
</ul>


</section>




    <section class="slide" id="elements-blockquotes">
      <h2>Other Elements: Blockquotes</h2>
      <blockquote cite="esrf">
        <p> Human behavior flows from three main sources: desire, emotion, and knowledge.</p>
        <p><cite>Plato</cite></p>
      </blockquote>
    </section>


    <section class="slide" id="elements-blockquotes">
      <h2>Other Elements: Blockquotes</h2>
      <blockquote cite="esrf">
        <p>I don't want to be at the mercy of my emotions. I want to use them, to enjoy them, and to dominate them.</p>
        <p><cite>Oscar Wilde</cite></p>
      </blockquote>
      <img src="images/emotion_layout.svg" alt="emotion layout">
    </section>




        <section class="slide" id="digging-deeper">
      <h2>About</h2>
        <ul>
          <li> <h3>Acknowledgments </h3>
            <ul>
              <li>Peter Cloetens ID16  </li>
              <li>Francois Villar : hexapod mechanical design</li>
            </ul>
          </li>
          <li>
            <h3>Project made with</h3>
             <ul>
               <li>  <a href="http://gitlab.esrf.fr/">git/gitlab</a>  </li>
               <li>  <a href="http://gitlab.esrf.fr/">Tango & PyTango & AtkPanel</a>  </li>
               <li>  <a href="http://gitlab.esrf.fr/">Python</a>  </li>
               </ul>
          </li>

          <li> <h3>Presentation made with  <a href="http://imakewebthings.com/deck.js/">deck.js</a></h3></li>

        </ul>
     </section>


    <!-- End slides. -->





    <!-- Begin extension snippets. Add or remove as needed. -->

    <!-- deck.navigation snippet -->
    <div aria-role="navigation">
      <a href="#" class="deck-prev-link" title="Previous">&#8592;</a>
      <a href="#" class="deck-next-link" title="Next">&#8594;</a>
    </div>

    <!-- deck.status snippet -->
    <p class="deck-status" aria-role="status">
      <span class="deck-status-current"></span>
      /
      <span class="deck-status-total"></span>
    </p>

    <!-- deck.goto snippet -->
    <form action="." method="get" class="goto-form">
      <label for="goto-slide">Go to slide:</label>
      <input type="text" name="slidenum" id="goto-slide" list="goto-datalist">
      <datalist id="goto-datalist"></datalist>
      <input type="submit" value="Go">
    </form>

    <!-- End extension snippets. -->
  </div>

<!-- Required JS files. -->
<script src="deck.js/jquery.min.js"></script>
<script src="deck.js/core/deck.core.js"></script>

<!-- Extension JS files. Add or remove as needed. -->
<script src="deck.js/extensions/menu/deck.menu.js"></script>
<script src="deck.js/extensions/goto/deck.goto.js"></script>
<script src="deck.js/extensions/status/deck.status.js"></script>
<script src="deck.js/extensions/navigation/deck.navigation.js"></script>
<script src="deck.js/extensions/scale/deck.scale.js"></script>

<!-- Initialize the deck. You can put this in an external file if desired. -->
<script>
  $(function() {
    $.deck('.slide');
  });
</script>
</body>
</html>
