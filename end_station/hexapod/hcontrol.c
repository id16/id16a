/*
 * hcontrol.c
 * cyril guilloud
 * fev 2012
 *
 * NINA-ni Piezo Hexapod control tests
 *
 */

/* std includes */
#include <stdio.h>
#include <sys/time.h>
#include <stdlib.h>
#include <unistd.h>


/* RDTSC */
#ifndef _RDTSC_H
#define _RDTSC_H

#include <sys/types.h>                  // for u_int64_t

/*
 not working with debian 6 gcc 4.6.2...??? is c++ ?
 #if !defined(i386) && !defined(__i386__)
 #error "Need an x86  for this"
 #endif
*/


/*
 * Reads the TSC (time-stamp counter) register of the processor.
 */
static inline u_int64_t rdtsc(void){
    u_int64_t rv;
    __asm __volatile(".byte 0x0f, 0x31" : "=A" (rv));
    return rv;
}

#endif

typedef struct{
    float sensor_values[8];
} sensor_val;

/* Reads capcitive sensor values */
int get_raw_capa_values(sensor_val* capa_sensor_values){
    capa_sensor_values->sensor_values[0] = 1.07;
    capa_sensor_values->sensor_values[1] = 1.16;
    capa_sensor_values->sensor_values[2] = 1.25;
    capa_sensor_values->sensor_values[3] = 1.34;
    capa_sensor_values->sensor_values[4] = 1.43;
    capa_sensor_values->sensor_values[5] = 1.52;
    capa_sensor_values->sensor_values[6] = 1.61;
    capa_sensor_values->sensor_values[7] = 1.70;

    return 0;
}

/* prints the vector */
int print_capa_values(sensor_val* capa_sensor_values){
    printf("capacitive sensor values=");
    for (int i=0; i<7; i++){
        printf("%7.3f ", capa_sensor_values->sensor_values[i]);
    }
    printf("\n");

    return 0;
}



int main(){
    // for gtod
    struct timeval *tv;
    struct timeval T_before;
    struct timeval T_after;

    printf("\n");
    printf("\n");

    /* RDTSC */
    printf("%ju\n", rdtsc());

    /* gtod */
    tv = malloc(sizeof(struct timeval));

    if(gettimeofday(tv, NULL) != 0 ){
        printf("error in get_time_of_day \n");
    }
    else{
        printf("time of day : %ld.%ld \n", tv->tv_sec, tv->tv_usec );
    }


    gettimeofday(&T_before, NULL);

    /* matrix bitouillage */
    sensor_val raw_capa;
    sensor_val avg_capa;
    sensor_val theta_0;
    float shape_error_table[8][72];  // 360/5 values to interpolate.
    sensor_val shape_error;            // interpolated values.
    float S_matrix[8][5];      // pre-calculated matrix for conversion [capa <-> Tx Ty Tz Rx Ry Rz].
    float mvt_error[6];        // measured error on movement.
    float mvt_consigne[6];     // [Tx Ty Tz Rx Ry Rz] movement consigne.
    float H_matrix[6][6];      // pre-calculated matrix for conversion [ Tx Ty Tz Rx Ry Rz <-> piezo legs].
    float piezo_correction[6]; // correction to apply to piezo legs to correct mvt errors.

    get_raw_capa_values(&raw_capa);
    print_capa_values(&raw_capa);
    /*
    for (int i=0; i<5; i++){
        for (int j=0; j<5; j++){
            m1[i][j]=i+j;
            m2[i][j]=i+j;
        }
    }

    for (int i=0; i<5; i++){
        for (int j=0; j<5; j++){

            m3[i][j] = 0;

            for (int k=0; k<5;k++){
                m3[i][j] +=  m1[i][k] * m2[k][j] ;
            }
        }
    }
    */
    gettimeofday(&T_after, NULL);

    printf("duration : %ld us    %f ms \n", ((T_after.tv_sec * 1000000 + T_after.tv_usec)
                                             - (T_before.tv_sec * 1000000 + T_before.tv_usec)),
           ((T_after.tv_sec * 1000.0 + T_after.tv_usec/1000.0)
            - (T_before.tv_sec * 1000.0 + T_before.tv_usec/1000.0))); 


    /*   // print result 
    for (int i=0; i<5; i++){
        for (int j=0; j<5; j++){
            printf("%7.3f  ", m3[i][j]);
        }
        printf("\n");
    }*/

    printf("\n");
    printf("\n");
    return 0;
}

