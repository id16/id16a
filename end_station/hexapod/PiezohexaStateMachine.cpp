/*----- PROTECTED REGION ID(PiezohexaStateMachine.cpp) ENABLED START -----*/
static const char *RcsId = "$Id:  $";
//=============================================================================
//
// file :        PiezohexaStateMachine.cpp
//
// description : C++ source for the Piezohexa and its alowed
//               methods for commands and attributes
//
// project :     .
//
// $Author:  $
//
// $Revision:  $
// $Date:  $
//
// SVN only:
// $HeadURL:  $
//
// CVS only:
// $Source:  $
// $Log:  $
//
//=============================================================================
//                This file is generated by POGO
//        (Program Obviously used to Generate tango Object)
//=============================================================================



#include <Piezohexa.h>
#include <PiezohexaClass.h>

/*----- PROTECTED REGION END -----*/


/*
 * Piezohexa states description:
 *
 */

namespace Piezohexa_ns
{
//=================================================
//		Attributes Allowed Methods
//=================================================

//--------------------------------------------------------
/**
 *	Method      : Piezohexa::is_CloseLoopEnabledState_allowed()
 *	Description : Execution allowed for CloseLoopEnabled attribute.
 */
//--------------------------------------------------------

bool Piezohexa::is_CloseLoopEnabled_allowed(TANGO_UNUSED(Tango::AttReqType type))
{
	//	Check if access type.
	if ( type!=Tango::READ_REQ )
	{
			//	Not any excluded states for CloseLoopEnabled attribute in WRITE access.
		
	/*----- PROTECTED REGION ID(Piezohexa::CloseLoopEnabledStateAllowed_WRITE) ENABLED START -----*/

	/*----- PROTECTED REGION END -----*/	//	Piezohexa::CloseLoopEnabledStateAllowed_WRITE

		return true;
	}
	else
		//	Not any excluded states for CloseLoopEnabled attribute in READ access.
	
	/*----- PROTECTED REGION ID(Piezohexa::read_CloseLoopEnabledStateAllowed_READ) ENABLED START -----*/

	/*----- PROTECTED REGION END -----*/	//	Piezohexa::read_CloseLoopEnabledStateAllowed_READ

	return true;
}

//--------------------------------------------------------
/**
 *	Method      : Piezohexa::is_SpeedState_allowed()
 *	Description : Execution allowed for Speed attribute.
 */
//--------------------------------------------------------

bool Piezohexa::is_Speed_allowed(TANGO_UNUSED(Tango::AttReqType type))
{
	//	Check if access type.
	if ( type!=Tango::READ_REQ )
	{
			//	Not any excluded states for Speed attribute in WRITE access.
		
	/*----- PROTECTED REGION ID(Piezohexa::SpeedStateAllowed_WRITE) ENABLED START -----*/

	/*----- PROTECTED REGION END -----*/	//	Piezohexa::SpeedStateAllowed_WRITE

		return true;
	}
	else
		//	Not any excluded states for Speed attribute in READ access.
	
	/*----- PROTECTED REGION ID(Piezohexa::read_SpeedStateAllowed_READ) ENABLED START -----*/

	/*----- PROTECTED REGION END -----*/	//	Piezohexa::read_SpeedStateAllowed_READ

	return true;
}

//--------------------------------------------------------
/**
 *	Method      : Piezohexa::is_PositionState_allowed()
 *	Description : Execution allowed for Position attribute.
 */
//--------------------------------------------------------

bool Piezohexa::is_Position_allowed(TANGO_UNUSED(Tango::AttReqType type))
{
	//	Check if access type.
	if ( type!=Tango::READ_REQ )
	{
			//	Not any excluded states for Position attribute in WRITE access.
		
	/*----- PROTECTED REGION ID(Piezohexa::PositionStateAllowed_WRITE) ENABLED START -----*/

	/*----- PROTECTED REGION END -----*/	//	Piezohexa::PositionStateAllowed_WRITE

		return true;
	}
	else
		//	Not any excluded states for Position attribute in READ access.
	
	/*----- PROTECTED REGION ID(Piezohexa::read_PositionStateAllowed_READ) ENABLED START -----*/

	/*----- PROTECTED REGION END -----*/	//	Piezohexa::read_PositionStateAllowed_READ

	return true;
}


//=================================================
//	Dynamic Attributes Allowed Methods
//=================================================


	/*----- PROTECTED REGION ID(Piezohexa::are_dynamic_attributes_allowed) ENABLED START -----*/

	//	Add your code to check if dynamic attributes are alowed

	/*----- PROTECTED REGION END -----*/	//	Piezohexa::are_dynamic_attributes_allowed


//=================================================
//		Commands Allowed Methods
//=================================================


//--------------------------------------------------------
/**
 *	Method      : Piezohexa::is_MoveState_allowed()
 *	Description : Execution allowed for Move command.
 */
//--------------------------------------------------------

bool Piezohexa::is_Move_allowed(TANGO_UNUSED(const CORBA::Any &any))
{
	//	Not any excluded states for Move command.

	/*----- PROTECTED REGION ID(Piezohexa::MoveStateAllowed) ENABLED START -----*/

	/*----- PROTECTED REGION END -----*/	//	Piezohexa::MoveStateAllowed

	return true;
}

//--------------------------------------------------------
/**
 *	Method      : Piezohexa::is_OnState_allowed()
 *	Description : Execution allowed for On command.
 */
//--------------------------------------------------------

bool Piezohexa::is_On_allowed(TANGO_UNUSED(const CORBA::Any &any))
{
	//	Not any excluded states for On command.

	/*----- PROTECTED REGION ID(Piezohexa::OnStateAllowed) ENABLED START -----*/

	/*----- PROTECTED REGION END -----*/	//	Piezohexa::OnStateAllowed

	return true;
}

//--------------------------------------------------------
/**
 *	Method      : Piezohexa::is_OffState_allowed()
 *	Description : Execution allowed for Off command.
 */
//--------------------------------------------------------

bool Piezohexa::is_Off_allowed(TANGO_UNUSED(const CORBA::Any &any))
{
	//	Not any excluded states for Off command.

	/*----- PROTECTED REGION ID(Piezohexa::OffStateAllowed) ENABLED START -----*/

	/*----- PROTECTED REGION END -----*/	//	Piezohexa::OffStateAllowed

	return true;
}


	/*----- PROTECTED REGION ID(Piezohexa::are_dynamic_commands_allowed) ENABLED START -----*/

	//	Add your code to check if dynamic commands are alowed

	/*----- PROTECTED REGION END -----*/	//	Piezohexa::are_dynamic_commands_allowed

}	// namespace Piezohexa_ns
