//////////////////////////////////////////////
//
// Multiplexeur Program for ID16 NI OPIOM Rack
//
//////////////////////////////////////////////

// INPUT
wire MUSST_OUTA;
wire MUSST_OUTB;
wire P201_GATEOUT;

// OUTPUT
reg  OPIOM_EXP_I1;

// ZAP Point or Spec Count
wire SEL_ACQ_TRIG;

// Input/Output Assignment
assign MUSST_OUTA   = I1;  // Input from Musst OUTA [ZAP Point Pulse]
assign MUSST_OUTB   = I2;  // Input from Musst OUTB [ZAP Point Gate]
assign P201_GATEOUT = I3;  // Input from P201 GATEOUT [SPEC count]

assign O1 = MUSST_OUTA;    // P201 EXTTRIG [ZAP Point Pulse]
assign O2 = OPIOM_EXP_I1;  // OPIOM EXP I1 [ZAP Point Pulse or P201 GATEOUT]
assign O3 = MUSST_OUTB;    // OPIOM EXP I1 [ZAP Point Gate]

// Register Assignement
assign SEL_ACQ_TRIG = IM1; // ZAP Point or Spec Count Selector

// source Acquisition Trigger
always @(SEL_ACQ_TRIG or MUSST_OUTA or P201_GATEOUT)
  begin
     case (SEL_ACQ_TRIG)
       1'b1 :    OPIOM_EXP_I1 = MUSST_OUTA;    // ZAP Point Pulse
       default : OPIOM_EXP_I1 = P201_GATEOUT;  // SPEC Count Gate
     endcase
  end



               
