//////////////////////////////////////////////
//
// Multiplexeur Program for ID16 NA OPIOM Rack
//
//////////////////////////////////////////////

// INPUT
wire ACQ_TRIG;
wire HEXA_PIEZO;
wire FRELON_SOR_1;
wire FRELON_SOR_2;
wire MUSST_OUTB;
wire FRELON_SHUT1;
wire FRELON_SHUT2;

// OUTPUT
reg SHUTTER;
reg MUSST_TRIG;

// Software Shutter Command
wire SOFT_SHUTTER;

// ZAP Point or Spec Count
wire [1:0]SEL_SHUTTER;
wire [1:0]SEL_MUSST_TRIG;

// Input/Output Assignment
assign ACQ_TRIG      = I1; // Input from OPIOM RACK O2 [ZAP Point Pulse or P201 GATEOUT]
assign HEXA_PIEZO    = I2; // Input from HexaPiezo [LTRIG]
assign FRELON_SOR_1  = I3; // Input from FRELON 1 SOR [Frelon 1 End Of Read]
assign FRELON_SOR_2  = I4; // Input from FRELON 2 SOR [Frelon 2 End Of Read]
assign MUSST_OUTB    = I5; // Input from OPIOM RACK O3 [Zap Point Gate]
assign FRELON_SHUT_1 = I7; // Input from FRELON 1 SHUTTER [Frelon 1 Shutter]
assign FRELON_SHUT_2 = I8; // Input from FRELON 2 SHUTTER [Frelon 2 Shutter]

assign O1 = ACQ_TRIG;      // ACQ_TRIG to Frelon 1 [ZAP Point Pulse or SPEC Count]
assign O2 = ACQ_TRIG;      // ACQ_TRIG to Frelon 2 [ZAP Point Pulse or SPEC Count]
assign O3 = ACQ_TRIG;      // ACQ_TRIG to Maxipix  [ZAP Point Pulse or SPEC Count]
assign O4 = ACQ_TRIG;      // ACQ_TRIG to Vistek   [ZAP Point Pulse or SPEC Count]
assign O5 = ACQ_TRIG;      // ACQ_TRIG to XMAP     [ZAP Point Pulse or SPEC Count]
assign O7 = SHUTTER;       // SHUTTER to device    [Shutter Control]
assign O8 = MUSST_TRIG;    // Envelop from HexapPiezo Movement or Frelon SOR

// Register Assignement
assign SHUTTER_SOFT        = IM1;        // Software Shutter Command
assign SEL_SHUTTER[1:0]    = {IM3, IM2}; // ZAP Point or Spec Count Selector
assign SEL_MUSST_TRIG[1:0] = {IM5, IM4}; // ZAP Point or Spec Count Selector

// source Shutter Control Selector
always @(SEL_SHUTTER or ACQ_TRIG or FRELON_SHUT_1 or FRELON_SHUT_2 or SOFT_SHUTTER)
  begin
     case (SEL_SHUTTER)
       2'b01 :   SHUTTER = FRELON_SHUT_1; 
       2'b10 :   SHUTTER = FRELON_SHUT_2; 
       2'b11 :   SHUTTER = SOFT_SHUTTER; 
       default : SHUTTER = ACQ_TRIG;  // SPEC Count Gate
     endcase
  end

// source MUSST ITRIG Selector
always @(SEL_MUSST_TRIG or HEXA_PIEZO or FRELON_SOR_1 or FRELON_SOR_2)
  begin
     case (SEL_MUSST_TRIG)
       2'b01 :   MUSST_TRIG = FRELON_SOR_1; 
       2'b10 :   MUSST_TRIG = FRELON_SOR_2;
       default : MUSST_TRIG = HEXA_PIEZO;  
     endcase
  end



               
